<?php

function watchdog_recurrent_drush_command() {
  $items = [];

  $items['watchdog-recurrent-notify'] = [
    'callback' => 'drush_watchdog_recurrent',
    'aliases' => array('wrn'),
    'options' => array(
      'limit' => "Optional. Minimal repetitions time to include an error to the report. Defaults to value set in the module configuration page.",
      'period' => "Optional. Period of time in hours to watch recurrent errors for. Defaults to value set in the module configuration page.",
      'mail' => "Optional. An e-mail address to send the report to."
    ),
    'examples' => array(
      'drush watchdog-recurrent-notify --period=24' => 'Send report for recurrent errors during last 24 hours. Limit and mail are default (set in the module configuration)',
      'drush watchdog-recurrent-notify --limit=100' => 'Send report for recurrent errors with limit set to 100',
      'drush watchdog-recurrent-notify --mail=mail@example.site' => 'Send report for recurrent errors to a custom e-mail address (mail@example.site)',
    ),
    'description' => t('Discovers recurrent errors in watchdog and sends a report to a given e-mail.'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  ];

  return $items;
}

function drush_watchdog_recurrent() {
  $limit = drush_get_option('limit');
  $period = drush_get_option('period');
  $mail = drush_get_option('mail');

  if ($mail) {
    $values = array_filter(explode(', ', $mail));
    foreach ($values as $value) {
      if (!valid_email_address(trim($value))) {
        drush_log(t('@mail is invalid. Interrupt...', ['@mail' => $value]), 'error');
      }
    }
  }

  $options = [
    'limit' => $limit,
    'period' => $period,
    'mail' => $mail,
  ];
  watchdog_recurrent_notify($options, TRUE);
}
