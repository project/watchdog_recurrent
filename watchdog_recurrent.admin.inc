<?php

/**
 * @file
 * Contains administration pages callbacks.
 */

/**
 * Page callback function for repeating errors report
 *
 * @return array
 */
function watchdog_recurrent_page() {
  $logs = watchdog_recurrent_get_logs();

  $header = [
    t('Errors count'),
    t('Log text'),
    t('Location'),
    t('Log example link'),
  ];
  $rows = [];

  /**
   * @see dblog_overview().
   */
  $classes = [
    WATCHDOG_DEBUG => 'dblog-debug',
    WATCHDOG_INFO => 'dblog-info',
    WATCHDOG_NOTICE => 'dblog-notice',
    WATCHDOG_WARNING => 'dblog-warning',
    WATCHDOG_ERROR => 'dblog-error',
    WATCHDOG_CRITICAL => 'dblog-critical',
    WATCHDOG_ALERT => 'dblog-alert',
    WATCHDOG_EMERGENCY => 'dblog-emerg',
  ];

  foreach ($logs as $log) {
    $location = text_summary($log->location, NULL, 100);
    if ($location < $log->location) {
      $location .= '...';
    }
    $rows[] = [
      'data' => [
        $log->count,
        t($log->message, unserialize($log->variables)),
        l($location, $log->location),
        l(t('view', [], ['context' => 'watchdog_recurrent']), 'admin/reports/event/' . $log->wid),
      ],
      'class' => [
        drupal_html_class('dblog-' . $log->type),
        $classes[$log->severity],
      ],
    ];
  }

  $output = [
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No data for chosen limits'),
    // We want similar styling to the dblog table.
    '#attributes' => ['id' => 'admin-dblog'],
  ];

  return $output;
}
